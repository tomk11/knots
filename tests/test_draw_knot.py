import unittest
import makeKnotGraph

wrapped_crossing_indexes = [0, 100, 200, 3, 380, 546, 611, 700]
undercrossings = [1, 3, 5]
overcrossings = [2, 4, 6]

class GetSegmentStartEndTests(unittest.TestCase):
    def test_first_segment(self):
        start, end = makeKnotGraph.get_segment_start_and_end_points(0, [], [1], wrapped_crossing_indexes)
        self.assertEqual(start, 0)
        self.assertGreater(end, 0)
        self.assertLess(end, 100)
