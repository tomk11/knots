from forceDirectedGraph import Vector
import unittest

class VectorTests(unittest.TestCase):
    def test_vector_addition(self):
        v1 = Vector(1,2)
        v2 = Vector(10,20)
        self.assertEqual(v1 + v2, Vector(11,22))