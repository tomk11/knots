import random
import pygame
import time
import matplotlib
import math

class Vector:
    def __init__(self, *x):
        self.x = x

    def __add__(self, other):
        return Vector(*tuple(map(sum, zip(self.x, other.x))))

    def __sub__(self, other):
        return Vector(*tuple(map(lambda x: x[0]-x[1], zip(self.x, other.x))))

    def __abs__(self):
        return sum(list(map(lambda x:x**2, self.x)))**0.5

    def __str__(self):
        return str(tuple(map(lambda x:'{:0.2f}'.format(x), self.x)))

    def __mul__(self, other):
        return Vector(*tuple(map(lambda x: other*x, self.x)))

    def __rmul__(self, other):
        return Vector(*tuple(map(lambda x: other*x, self.x)))

    def __div__(self, other):
        return Vector(*tuple(map(lambda x: x/other, self.x)))

    def __truediv__(self, other):
        return Vector(*tuple(map(lambda x: x/other, self.x)))

    def normalize(self):
        return self/abs(self)

    def rotate(self, angle):
        angle = math.radians(angle)
        c = math.cos(angle)
        s = math.sin(angle)
        x = self.x[0]
        y = self.x[1]
        return Vector(x * c - y * s, x * s + y * c )
    
    def __repr__(self):
        return 'Vector: ' + str(self.x)
    
    def __neg__(self):
        return  Vector(*tuple(map(lambda x: -x[0], self.x)))

    def __eq__(self, other):
        return self.x == other.x

def coulomb_force(d, beta=0.1):
    d_squared = d **  2
    if d_squared == 0.0:
        force = 0
    else:
        force = beta / (d_squared)
    return force

def hooke_force(length, spring_constant=1, natural_length=1):
    extension = length - natural_length
    return - spring_constant * extension / length   

def dist(x, y):
    return abs((x[0]-y[0], x[1]-y[1]))

def on_segment(p, q, r):
    ''' Given three colinear points p, q, r, the function checks if point q lies on line segment 'pr' '''
    return (q[0] <= max(p[0], r[0]) and 
            q[0] >= min(p[0], r[0]) and 
            q[1] <= max(p[1], r[1]) and 
            q[1] >= min(p[1], r[1]))

def find_orientation(p, q, r):
    # To find orientation of ordered triplet (p, q, r).
    # The function returns following values
    # 0: p, q and r are colinear, 1: Clockwise, 2: Counterclockwise
    # See http://www.geeksforgeeks.org/orientation-3-ordered-points/
    val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])
    if val == 0:
        return 0  #colinear
    elif val >0:
        return 1
    else:
        return 2
 
def do_intersect(p1, q1, p2, q2):
    '''returns true if line segment 'p1q1' and 'p2q2' intersect'''

    # Find the four orientations needed for general and special cases
    o1 = find_orientation(p1, q1, p2)
    o2 = find_orientation(p1, q1, q2)
    o3 = find_orientation(p2, q2, p1)
    o4 = find_orientation(p2, q2, q1)
 
    # General case
    if (o1 != o2 and o3 != o4):
        return True
 
    # Special Cases
    # p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 and on_segment(p1, p2, q1)):
        return True
 
    # p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 and on_segment(p1, q2, q1)):
        return True
 
    # p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 and on_segment(p2, p1, q2)):
        return True
 
     # p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 and on_segment(p2, q1, q2)):
        return True
 
    return False # Doesn't fall in any of the above cases
 
class Edge:
    def __init__(self, vertex1, vertex2, name=None):
        self.vertex1 = vertex1
        self.vertex2 = vertex2 
        self.name = name

    def __str__(self):
        return '(' + str(self.vertex1) + ', ' + str(self.vertex2) + ')'

class Vertex:
    def __init__(self, name):
        self.name = name
        self.location = Vector(random.random(), random.random())
        self.render_location = Vector(0,0)
        self.adjacent = []
        self.edges = []
        self.speed = Vector(0,0)

    def __str__(self):
        return str(self.name)

class Graph:
    def __init__(self):
        self.verticies = []  
        self.edges = []
        self.vertex_dict = {}
        self.edgeDict = {}
        self.background_colour = (255, 255, 255)
        self.width, self.height = 500, 500
        self.screen = None
        self.colour = (0, 0, 255)
        self.sum_square_speed = 1

    def are_edges_adjacent(self, e1, e2):
        return (e1.vertex1 is e2.vertex1 
                or e1.vertex1 is e2.vertex2 
                or e1.vertex2 is e2.vertex1 
                or e1.vertex2 is e2.vertex2)

    def get_non_adjacent_edges(self):
        non_adjacent_edges = []
        n = len(self.edges)
        for i in range(n):
            for j in range(i+1,n):
                if not self.are_edges_adjacent(self.edges[i], self.edges[j]):
                    non_adjacent_edges.append((self.edges[i], self.edges[j]))
        return non_adjacent_edges

    def add_verticies(self, *verticies):
        for v in verticies:
            self.add_vertex(v)

    def add_vertex(self, vertex):
        v = Vertex(vertex)
        self.verticies.append(v)
        self.vertex_dict[str(vertex)] = v

    def add_edges(self, *edges):
        for e in edges:
            self.add_edge(e[0], e[1])

    def add_edge(self, v1, v2, name=None):
        # lookup vertext objects from names
        v1_obj = self.vertex_dict[str(v1)]
        v2_obj = self.vertex_dict[str(v2)]

        # create an edge
        e =  Edge(v1_obj, v2_obj, name)

        # add edges to all the relevant lookup dicts
        self.edges.append(e)
        v1_obj.edges.append(e)
        v2_obj.edges.append(e)
        self.edgeDict[str(v1)] = v2_obj
        self.edgeDict[str(v2)] = v1_obj
        v1_obj.adjacent.append(v2_obj)
        v2_obj.adjacent.append(v1_obj)

    def __str__(self):
        return str((self.vertex_dict, self.edgeDict, self.verticies, self.edges))

    def random_coords(self):
        for v in self.verticies:
            v.location = Vector(random.random(), random.random())

    def is_crossing_number_zero(self):
        ''' checks whether graph has crossing number 0 by iterating throuh non-adjacent edges 
        and failing if these intersect'''
        non_adjacent_edges = self.get_non_adjacent_edges()
        for edge_pair in non_adjacent_edges:
            if self.intersects(edge_pair[0], edge_pair[1]):
                return False
        return True

    def iterate(self):
        # measure of whether we are close to equilibrium
        self.sum_square_speed = 0

        for v in self.verticies:
            v.speed = Vector(0,0)

            # Apply hookes law on all adjacent verticies
            for vn in v.adjacent:
                hooke = hooke_force(abs(v.location - vn.location))
                v.speed += hooke * ((v.location-vn.location).normalize())

            # Apply coulombs law on all verticies
            for v2 in self.verticies:
                if v is not v2:
                    coulomb = coulomb_force(abs(v.location - v2.location))
                    direction = (v.location - v2.location).normalize()
                    change = coulomb * ((v.location-v2.location).normalize())
                    v.speed += coulomb * ((v.location-v2.location).normalize())

            v.location += v.speed/10
            self.sum_square_speed += abs(v.speed) **2
        return self.sum_square_speed

    def intersects(self, e1, e2):
        return do_intersect(e1.vertex1.location.x, e1.vertex2.location.x, e2.vertex1.location.x, e2.vertex2.location.x) 
         
    def get_coords(self, height, width, border_fraction=0.2):
        xmin = min(list(map(lambda v:v.location.x[0], self.verticies)))
        xmax = max(list(map(lambda v:v.location.x[0], self.verticies)))
        ymin = min(list(map(lambda v:v.location.x[1], self.verticies)))
        ymax = max(list(map(lambda v:v.location.x[1], self.verticies)))

        for v in self.verticies:
            v.renderLocation = (1-2*border_fraction) * Vector((v.location.x[0] -xmin)/(xmax-xmin)*height, (v.location.x[1] -ymin)/(ymax-ymin)*width) + border_fraction * (Vector(height, width))

    def calculate_valid_graph(self, view=False):
        tolerance = 10**-4

        while not(self.is_crossing_number_zero()) or self.sum_square_speed > tolerance:
            if self.sum_square_speed < tolerance:
                self.random_coords()
            
            self.iterate()
            self.get_coords(500,500)

            if view:
                self.render_graph()

    def render_graph(self):
        if self.screen == None:
            self.screen = pygame.display.set_mode((self.width, self.height))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                break

        self.screen.fill(self.background_colour)
        for v in self.verticies:
            pygame.draw.circle(self.screen, self.colour, (int(v.renderLocation.x[0]), int(v.renderLocation.x[
        1])), 8, 1)

        for e in self.edges:
            pygame.draw.line(self.screen, self.colour, e.vertex1.renderLocation.x, e.vertex2.renderLocation.x, 4)

        pygame.display.flip()
