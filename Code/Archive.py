import fcntl, os, subprocess, time

''' INTERFACING WITH MATHEMATICA
From a mathematical perspective this wasn't too interesting. This section of the code is concerned with interfacing with mathematica. We use the python subprocess module to run the math command and load the knot theory package. We pipe input and output from the process and we have to set this as non-blocking to allow asynchronous communication. Sometimes we have sleep to wait for mathematic to finish. We ask mathematica to return fortran object which are closer to sage syntax. We then have to do some string parsing to do the rest.
'''
mathematica = subprocess.Popen(['math'], shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
flags = fcntl.fcntl(mathematica.stdout, fcntl.F_GETFL)
fcntl.fcntl(mathematica.stdout, fcntl.F_SETFL, flags | os.O_NONBLOCK)
mathematica.stdin.write(('<<KnotTheory` \n').encode('utf-8'))
mathematica.stdin.flush()

def math(command):
    time.sleep(0.5)
    mathematica.stdin.write(('ToString[' + command + ', InputForm]\n').encode('utf-8'))
    out = ''
    while('Out' not in out):
        try:
            time.sleep(0.01)
            mathematica.stdin.flush()
            mathematica.stdout.flush()
            out =mathematica.stdout.readline().decode("utf-8")
        except:
            pass
    out = out[out.find('=') +2 :-1]
    return out

def sagify(expr, ls):
    expr = expr.replace("Sqrt[", "sqrt(")
    expr = expr.replace("]", ")")
    return sage_eval(expr, locals = ls)

def parse(s):
    if s[-2:] == '\n':
        s = s[:-2]
    s = s + ' '
    t = var('t')
    jones = 0
    KnotKey = s[0] + '_' + s[1:s.find(':')]
    if '{' in s:
        exponent = int(s[s.find('{')+1:s.find('}')])
        s = s[s.find('}') +1:]
    else:
        s = s[s.find(":")+2: ]
        exponent = - s[:s.find(']')].count(' ')
        s = s[:s.find('[') ] + s[s.find('[') + 1: s.find(']') ] + s[s.find(']') + 1:]

    current = ''
    for digit in s:
        if digit == ' ':
            if current != '':
                jones += int(current) * t ^ exponent
                exponent += 1
            current = ''
        else:
            current += digit
    return((KnotKey,jones))

def run():
    os.system('tail -f mmpipe | math -noprompt -run "<<KnotTheory\`" > outpipe &')
    fifo = open('outpipe', 'r')
    return (fifo.readline())

def wolfram(cmd):
    os.system('echo ' + cmd + ' > mmpipe')
    fifo = open('outpipe', 'r')
    return (fifo.readline())

def jones(DTCode):
    return (wolfram('Jones[DTCode' + str(DTCode) + '][q]'))

def getComponents(PD):
    components = []
    while sum(components) < 2 * len(PD):
        components.append(getComponentFromStartPoint(PD, sum(components) + 1))
    return(components)

def getComponent(PD, arcNo):
    components = getComponents(PD)
    for c in components:
        arcNo -= c
        if arcNo <=0:
            return c

def getComponentNo(PD, arcNo):
    components = getComponents(PD)
    for i in range(len(components)):
        arcNo -= components[i]
        if arcNo <=0:
            return i

def getComponentStartpoint(PD, arcNo):
    components = getComponents(PD)
    startpoint = 1
    for i in range(len(components)):
        arcNo -= components[i]
        if arcNo <=0:
            return startpoint
        startpoint += components[i]

def makeFullCode(DTCode):
    FullCode = []
    index = 0
    while index < len(DTCode):
        sign = -abs(DTCode[index]) // DTCode[index]
        FullCode.append((sign * (2 * index + 1), DTCode[index]))
        FullCode.append((DTCode[index], sign * (2 * index + 1)))
        index += 1
    FullCode = sorted(FullCode, key=lambda pair: abs(pair[0]))
    return FullCode

def makeDTCode(FullCode, LinkPos=0):
    if LinkPos == 0:
        DTCode = []
        for i in range(len(FullCode) // 2):
            DTCode.append(FullCode[2 * i][1])
        return DTCode
    else:
        DTCode = [[], []]
        for i in range(LinkPos):
            DTCode[0].append(FullCode[2 * i][1])
        for i in range(LinkPos, len(FullCode) // 2):
            DTCode[1].append(FullCode[2 * i][1])
        return DTCode

def makeDTString(DTCode):
    if type(DTCode[0]) == list:
        return '[{' + str(DTCode[0])[1:-1] + '},{' + str(DTCode[1])[1:-1] + '}]'
    else:
        return str(DTCode)

def switchCrossing(DTCode, crossingNo):
    if crossingNo in DTCode:
        listIndex = DTCode.index(crossingNo)
    else:
        listIndex = (crossingNo - 1) // 2
    modifiedDTCode = DTCode
    modifiedDTCode[listIndex] = -modifiedDTCode[listIndex]
    return modifiedDTCode

def typeOne(DTCode, removedCrossingNo):
    fullCode = makeFullCode(DTCode)

    # we ensure that we start our algorithm from the first place we encounter the crossing in question
    removedCrossing = fullCode[removedCrossingNo - 1]
    if abs(removedCrossing[1]) < abs(removedCrossing[0]):
        removedCrossingNo = abs(removedCrossing[1])
        removedCrossing = removedCrossing[::-1]

    # We then replace the crossing
    newFullCode = []

    i = 1  # index
    while i <= len(fullCode):
        crossing = fullCode[i - 1]
        newCrossing = (
        crossingMapTypeOne(crossing[0], removedCrossing), crossingMapTypeOne(crossing[1], removedCrossing))
        if newCrossing != (None, None):
            newFullCode.append(newCrossing)
        i += 1

    newFullCode = sorted(newFullCode, key=lambda pair: abs(pair[0]))
    newDTCode = makeDTCode(newFullCode)
    return newDTCode

def crossingMapTypeOne(index, removedCrossing):
    firstRemovedIndex = abs(removedCrossing[0])
    secondRemovedIndex = abs(removedCrossing[1])

    # crossing index is unaffected
    if abs(index) < firstRemovedIndex:
        return index

    # crossing doesn't exist any more
    elif abs(index) == firstRemovedIndex:
        return None

    # crossing lies on loop firstcrossing --> second crossing
    elif abs(index) < secondRemovedIndex:
        # crossing orientation is maintained
        sign = abs(index) / index
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        return int(sign * (firstRemovedIndex - 1 + (secondRemovedIndex - abs(index))))

    # crossing doesn't exist any more
    elif abs(index) == secondRemovedIndex:
        return None
        # crossing lies on loop firstcrossing --> second crossing

    elif abs(index) > secondRemovedIndex:
        # crossing orientation is maintained
        sign = abs(index) / index
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        return int(sign * (abs(index) - 2))

def typeTwo(DTCode, removedCrossingNo):
    fullCode = makeFullCode(DTCode)
    n = len(fullCode)

    # we ensure that we start our algorithm from the first place we encounter the crossing in question
    removedCrossing = fullCode[removedCrossingNo - 1]
    if abs(removedCrossing[1]) < abs(removedCrossing[0]):
        removedCrossingNo = abs(removedCrossing[1])
        removedCrossing = removedCrossing[::-1]

    LinkOneSize = (n - 1 - abs(removedCrossing[1]) + abs(removedCrossing[0])) // 2

    # We then replace the crossing
    newFullCode = []

    i = 1  # index
    while i <= len(fullCode):
        crossing = fullCode[i - 1]
        newCrossing = (
        crossingMapTypeTwo(crossing[0], removedCrossing, n), crossingMapTypeTwo(crossing[1], removedCrossing, n))
        if newCrossing != (None, None):
            newFullCode.append(newCrossing)
        i += 1

    newFullCode = sorted(newFullCode, key=lambda pair: abs(pair[0]))
    newDTCode = makeDTCode(newFullCode, LinkOneSize)
    return newDTCode

def crossingMapTypeTwo(index, removedCrossing, n):
    firstRemovedIndex = abs(removedCrossing[0])
    secondRemovedIndex = abs(removedCrossing[1])

    # crossing index is unaffected
    if abs(index) < firstRemovedIndex:
        return index

    # crossing doesn't exist any more
    elif abs(index) == firstRemovedIndex:
        return None

    # crossing lies on loop firstcrossing --> second crossing
    elif abs(index) < secondRemovedIndex:
        # crossing orientation is maintained
        sign = abs(index) / index
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        new = int(sign * (n - 1 + abs(index) - secondRemovedIndex))
        return new

    # crossing doesn't exist any more
    elif abs(index) == secondRemovedIndex:
        return None
        # crossing lies on loop firstcrossing --> second crossing

    elif abs(index) > secondRemovedIndex:
        # crossing orientation is maintained
        sign = abs(index) / index
        # the points firstRemovedIndex...SecondRemovedIndex need to be subtracted
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        # print(index,int(sign * ( abs(index) - (secondRemovedIndex - firstRemovedIndex + 1))))
        return int(sign * (abs(index) - (secondRemovedIndex - firstRemovedIndex + 1)))


def getInvariant(DT, removedCrossing):
    # print(sagify(math('Jones[DTCode' + DTList[0] + '][t]'), ls={'t': t}))
    # print(sagify(math('Jones[DTCode' + DTList[1] + '][t]'), ls={'t': t}))
    # print(sagify(math('Jones[DTCode' + DTList[2] + '][t]'), ls={'t': t}))
    # print(sagify(math('Jones[DTCode' + DTList[3] + '][t]'), ls={'t': t}))
    #
    # print(generate_r(JonesList[0]))
    # print(generate_r(JonesList[1]))
    # print(generate_r(JonesList[2]))
    # print(generate_r(JonesList[3]))
    DTList = []
    DTList.append(makeDTString(DT))
    print(DT)
    print(makeDTString(DT))
    DTList.append(makeDTString(switchCrossing(DT, removedCrossing)))
    DTList.append(makeDTString(typeOne(DT, removedCrossing)))
    DTList.append(makeDTString(typeTwo(DT, removedCrossing)))
    print(DTList)

    JonesList = list(map(lambda DT: sagify(math('Jones[DTCode' + DT + '][t]'), ls={'t': t}), DTList))
    print(JonesList)

    RList = list(map(lambda Jones: generate_r(Jones), JonesList))
    print(RList)
    FinalR = expand(RList[0] + RList[1] + y*(RList[2] + RList[3]))
    return [DTList, JonesList, RList, FinalR]

#print(getInvariant(DT,5))
# J = {}
# Jones = ()
# READ JONES POLYNOMIALS INTO SAGE FROM A TEXT FILE
#f=open('Jones.txt')
#for line in f:
    # p = parse(line)
    # J[p[0]] = p[1]
    # Jones = Jones + ((p[0],p[1]),)
#
# GENERATE THE LATEX TABLE
#for j in Jones:
#   print("$" + str(j[0]) + "$ & $" + latex(j[1]) + "$ & $" + latex(generate_r(j[1])) + "$ \\\\")
#
# EXAMPLE CALCULATION
# r = expand(generate_r(J['7_5']) + generate_r(J['5_1']) + y * generate_r(J['6_2']) + y **3 * generate_r(J['3_1']))
# print(r)
# print(r.factor())