import os

def makeFullCode(DTCode):
    FullCode = []
    index = 0
    while index < len(DTCode):
        sign = -abs(DTCode[index])// DTCode[index]
        FullCode.append((sign *(2 * index + 1), DTCode[index]))
        FullCode.append( (DTCode[index], sign *(2 * index + 1)))
        index += 1
    FullCode = sorted(FullCode, key=lambda pair: abs(pair[0]))
    return FullCode

def makeDTCode(FullCode, LinkPos=0):
    if LinkPos == 0:
        DTCode = []
        for i in range(len(FullCode)//2):
            DTCode.append(FullCode[2*i][1])
        return DTCode
    else:
        DTCode = [[],[]]
        for i in range(LinkPos):
            DTCode[0].append(FullCode[2*i][1])
        for i in range(LinkPos, len(FullCode)//2):
            DTCode[1].append(FullCode[2*i][1])
        return DTCode

def makeDTString(DTCode):
    if type(DTCode[0]) == int:
        return(str(DTCode))
    else:
        return '[{' + str(DTCode[0])[1:-1] + '},{' + str(DTCode[1])[1:-1] + '}]'


def switchCrossing(DTCode, crossingNo):
    if crossingNo in DTCode:
        listIndex = DTCode.index(crossingNo)
    else:
        listIndex = (crossingNo - 1) // 2
    modifiedDTCode = DTCode
    modifiedDTCode[listIndex] = -modifiedDTCode[listIndex]
    return modifiedDTCode


def typeOne(DTCode, removedCrossingNo):
    fullCode = makeFullCode(DTCode)

    # we ensure that we start our algorithm from the first place we encounter the crossing in question
    removedCrossing =  fullCode[removedCrossingNo - 1]
    if abs(removedCrossing[1]) < abs(removedCrossing[0]):
        removedCrossingNo = abs(removedCrossing[1])
        removedCrossing = removedCrossing[::-1]

    # We then replace the crossing
    newFullCode = []
    
    i = 1 # index
    while i <= len(fullCode):
        crossing = fullCode[i-1]
        newCrossing = (crossingMapTypeOne(crossing[0], removedCrossing), crossingMapTypeOne(crossing[1], removedCrossing))
        if newCrossing != (None, None):
            newFullCode.append(newCrossing)
        i += 1

    newFullCode = sorted(newFullCode, key=lambda pair: abs(pair[0]))
    newDTCode = makeDTCode(newFullCode)
    return newDTCode

def crossingMapTypeOne(index, removedCrossing):
    firstRemovedIndex = abs(removedCrossing[0])
    secondRemovedIndex = abs(removedCrossing[1])

    # crossing index is unaffected
    if abs(index) < firstRemovedIndex:
        return index
    
    # crossing doesn't exist any more
    elif abs(index) == firstRemovedIndex:
        return None

    # crossing lies on loop firstcrossing --> second crossing
    elif abs(index) < secondRemovedIndex:
        #crossing orientation is maintained
        sign = abs(index)/ index
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        return int(sign * (firstRemovedIndex -1 + (secondRemovedIndex - abs(index))))
                       
    # crossing doesn't exist any more
    elif abs(index) == secondRemovedIndex:
        return None
        # crossing lies on loop firstcrossing --> second crossing

    elif abs(index) > secondRemovedIndex:
        #crossing orientation is maintained
        sign = abs(index)/ index
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        return int(sign * (abs(index) - 2))


def typeTwo(DTCode, removedCrossingNo):
    fullCode = makeFullCode(DTCode)
    n = len(fullCode)

    # we ensure that we start our algorithm from the first place we encounter the crossing in question
    removedCrossing = fullCode[removedCrossingNo - 1]
    if abs(removedCrossing[1]) < abs(removedCrossing[0]):
        removedCrossingNo = abs(removedCrossing[1])
        removedCrossing = removedCrossing[::-1]

    LinkOneSize = (n - 1 - abs(removedCrossing[1])  + abs(removedCrossing[0]))//2

    # We then replace the crossing
    newFullCode = []

    i = 1  # index
    while i <= len(fullCode):
        crossing = fullCode[i - 1]
        newCrossing = (crossingMapTypeTwo(crossing[0], removedCrossing,n), crossingMapTypeTwo(crossing[1], removedCrossing, n))
        if newCrossing != (None, None):
            newFullCode.append(newCrossing)
        i += 1

    newFullCode = sorted(newFullCode, key=lambda pair: abs(pair[0]))
    newDTCode = makeDTCode(newFullCode, LinkOneSize)
    return newDTCode


def crossingMapTypeTwo(index, removedCrossing,n):
    firstRemovedIndex = abs(removedCrossing[0])
    secondRemovedIndex = abs(removedCrossing[1])

    # crossing index is unaffected
    if abs(index) < firstRemovedIndex:
        return index

    # crossing doesn't exist any more
    elif abs(index) == firstRemovedIndex:
        return None

    # crossing lies on loop firstcrossing --> second crossing
    elif abs(index) < secondRemovedIndex:
        # crossing orientation is maintained
        sign = abs(index) / index
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        new = int(sign * (n  -1 + abs(index) - secondRemovedIndex))
        return new

    # crossing doesn't exist any more
    elif abs(index) == secondRemovedIndex:
        return None
        # crossing lies on loop firstcrossing --> second crossing

    elif abs(index) > secondRemovedIndex:
        # crossing orientation is maintained
        sign = abs(index) / index
        # the points firstRemovedIndex...SecondRemovedIndex need to be subtracted
        # -1 as one point crossing before has disappeared
        # firstRemovedCrossing is starting index
        # print(index,int(sign * ( abs(index) - (secondRemovedIndex - firstRemovedIndex + 1))))
        return int(sign * ( abs(index) - (secondRemovedIndex - firstRemovedIndex + 1)))

def run():
    os.system('tail -f mmpipe | math -noprompt -run "<<KnotTheory\`" > outpipe &')
    fifo = open('outpipe', 'r')
    return(fifo.readline())

    
def wolfram(cmd):
    os.system('echo ' +  cmd + ' > mmpipe')
    fifo = open('outpipe', 'r')
    return(fifo.readline())

def jones(DTCode):
    return(wolfram('Jones[DTCode' + str(DTCode) + '][q]'))

run()

DT = [-8,-10,-2, -12,-4, -6]
DT = [4,6,2]

print(makeDTString(DT))
print(makeDTString(switchCrossing(DT,5)))
print(makeDTString(typeOne(DT,5)))
print(makeDTString(typeTwo(DT,5)))

print('DT')
# print(jones(makeDTString(DT)))
# print(jones(makeDTString(switchCrossing(DT,5))))
# print(jones(makeDTString(typeOne(DT,5))))
# print(jones(makeDTString(typeTwo(DT,5))))
# print(wolfram('1+1'))
