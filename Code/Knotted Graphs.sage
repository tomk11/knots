# DECOMPOSE FUNCTION OF t TO A FUNCTION OF t+t^-1. Can be used on any polynomial with matching coefficients for corresponding +ve and -ve powers.
def decompose(r):
    if r ==1:
        return 1
    else:
        t = var('t')
        y = var('y')
        #r_y(y) = 0
        r_y = 0
        coefs = r(t).coefficients()
        while coefs != [] and coefs != [[0,0]]:
            a, m = coefs[-1]
            r -= a * (t + t^-1)^ m
            r_y += a * y ^ m
            coefs = r.coefficients()
        return r_y

def generate_r(jones):
    if jones ==1:
        return 1
    else:
        return decompose(simplify(expand(jones(t)*jones(t^-1))))

def parsePD(PDString):
    PDString = PDString.replace('PD', '').replace('X','')
    return eval(PDString)

def getComponentFromStartPoint(PD, startpoint):
    matches = []
    for crossing in PD:
        if crossing[0] == startpoint:
            matches.append(crossing[2])
        elif crossing[1] == startpoint:
            matches.append(crossing[3])
        elif crossing[2] == startpoint:
            matches.append(crossing[0])
        elif crossing[3] == startpoint:
            matches.append(crossing[1])
        if len(matches) == 2:
            return max(matches) + 1 - startpoint

def makeExplicitPDCode(PDCode):
    In = list(map(lambda point:point[0], PDCode))
    Out = list(map(lambda point:-point[2], PDCode))

    ExplicitPDCode = []
    for point in PDCode:
        newPoint = point[:]
        newPoint[2] = -newPoint[2]
        if newPoint[1] in In or -newPoint[3] in Out:
            newPoint[1] = -newPoint[1]
        if newPoint[3] in In or -newPoint[1] in Out:
            newPoint[3] = -newPoint[3]
        ExplicitPDCode.append(newPoint)
    return ExplicitPDCode

def implicitCrossing(crossing):
    return list(map(lambda value: abs(value), crossing))

def getPDCode(explicitPDCode):
    return list(map(lambda crossing: implicitCrossing(crossing), explicitPDCode))

def switchCrossing(PDCode, crossingNo):
    explicitPD = makeExplicitPDCode(PDCode)
    node = explicitPD[crossingNo]
    if node[3] > 0:
        node = node[3:] + node[:3]
    else:
        node = node[1:] + node[:1]
    explicitPD[crossingNo] = node
    return getPDCode(explicitPD)

def switchHandedness(PDCode):
    for i in range(len(PDCode)):
        PDCode = switchCrossing(PDCode, i)
    return PDCode

def typeOne(PDCode, crossingNo):
    # In the transformation, all knot sections preserve orientation

    ExplicitPD = makeExplicitPDCode(PDCode)
    deletedCrossing = ExplicitPD[crossingNo]
    incOne = deletedCrossing[0]
    outOne = -deletedCrossing[2]
    if deletedCrossing[1] > 0:
        incTwo = deletedCrossing[1]
        outTwo = -deletedCrossing[3]
    else:
        incTwo = deletedCrossing[3]
        outTwo = -deletedCrossing[1]


    replace = dict()
    if deletedCrossing[1] > 0:
        replace[deletedCrossing[1]] = deletedCrossing[2]
        replace[deletedCrossing[0]] = deletedCrossing[3]
    else:
        replace[deletedCrossing[3]] = deletedCrossing[2]
        replace[deletedCrossing[0]] = deletedCrossing[1]
    TempExplicitPD = []
    for crossing in ExplicitPD[:crossingNo] + ExplicitPD[1+ crossingNo:]:
        TempCrossing = []
        for arc in crossing:
            if abs(arc) in replace.keys():
                TempCrossing.append(-arc//abs(arc) * replace[abs(arc)])
            else:
                TempCrossing.append(arc)
        TempExplicitPD.append(TempCrossing)
    PD = reIndex(TempExplicitPD)

    if incOne == outTwo and outOne == incTwo:
        PD.append('Loop[1]')

    return PD

def typeTwo(PDCode, crossingNo):
    # In the transformation, a section in the middle of the knot changes orientation.
    ExplicitPD = makeExplicitPDCode(PDCode)
    reversedExplicitPD = [[i for i in j] for j in ExplicitPD]
    #print(ExplicitPD)

    deletedCrossing = ExplicitPD[crossingNo]

    # get incoming and outgoing parts from the crossing
    incOne = deletedCrossing[0]
    outOne = -deletedCrossing[2]
    if deletedCrossing[1] > 0:
        incTwo = deletedCrossing[1]
        outTwo = -deletedCrossing[3]
    else:
        incTwo = deletedCrossing[3]
        outTwo = -deletedCrossing[1]
    #trace the current path
    oldPath = dict()
    for crossing in ExplicitPD:
        oldPath[crossing[0]] = -crossing[2]
        if crossing[1] > 0:
            oldPath[crossing[1]] = -crossing[3]
        else:
            oldPath[crossing[3]] = -crossing[1]

    backPath = dict()
    for index in list(oldPath.keys()):
        backPath[oldPath[index]] = index

    #trace the new path, first add the changes, also keep edit the explicitPD as necessary
    newPath = dict()
    newPath[incOne] = backPath[incTwo]
    currentPos = backPath[incTwo]
    while currentPos not in (outOne, outTwo):
        newPath[currentPos] = backPath[currentPos]
        # look for the appropriate verticies in reversedExplicitPD and edit them accordingly
        for crossing in reversedExplicitPD:
            if currentPos in (crossing[0], crossing[2]):
                crossing[0], crossing[1], crossing[2], crossing[3] = -crossing[2],crossing[3], -crossing[0], crossing[1]
            elif currentPos in (crossing[1], crossing[3]):
                crossing[1], crossing[3] = -crossing[1], -crossing[3]
        currentPos = newPath[currentPos]
    if currentPos == outOne:
        newPath[currentPos] = oldPath[outTwo]
        currentPos = oldPath[outTwo]
    else:
        newPath[currentPos] = outOne
        currentPos = outOne
    #    print(oldPath, currentPos)
    # print('onward', newPath)
    while currentPos != incOne:
        newPath[currentPos] = oldPath[currentPos]
        currentPos = oldPath[currentPos]

    ExplicitPD = ExplicitPD[:crossingNo] + ExplicitPD[1 + crossingNo:]

    reMap = dict()
    pathKeys = list(newPath.keys())
    pos = pathKeys[0]
    i = 1
    while len(pathKeys) > 0:
        reMap[pos] = i
        pos = newPath[pos]
        i += 1
        pathKeys.remove(pos)
    reMap[incTwo] = reMap[incOne]
    reMap[outTwo] = reMap[outOne]

    implicitPD = list(map(lambda c:implicitCrossing(c), ExplicitPD))
    NewPD = [[reMap[n] for n in crossing] for crossing in implicitPD]

    if incOne == outTwo:
        NewPD.append('Loop[1]')

    if incTwo == outOne:
        NewPD.append('Loop[1]')

    return NewPD

def reIndex(ExplicitPD):
    currentLabels = []
    path = dict()
    for crossing in ExplicitPD:
        path[crossing[0]] = -crossing[2]
        if crossing[1] > 0:
            path[crossing[1]] = -crossing[3]
        else:
            path[crossing[3]] = -crossing[1]
    reIndexer = dict()
    for crossing in ExplicitPD:
        currentLabels += list(filter(lambda c: c>0, crossing))

    i = 1
    while len(currentLabels) > 0:
        n = currentLabels[0]
        while n in currentLabels:
            currentLabels.remove(n)
            reIndexer[n] = i
            n = path[n]
            i += 1
    implicitPD = list(map(lambda c:implicitCrossing(c), ExplicitPD))
    NewPD = [[reIndexer[n] for n in crossing] for crossing in implicitPD]
    return NewPD

def resolveNode(PD, node):
    PDs = [PD, switchCrossing(PD, node), typeOne(PD, node), typeTwo(PD, node)]
    return PDs

def makeMathematicaPD(PD):
    PDstr = "PD["
    for i in range(len(PD) -1):
        if type(PD[i]) == list:
            PDstr += 'X' + str(PD[i]) + ', '
        else:
            PDstr += str(PD[i]) + ', '
    if type(PD[-1]) == list:
        PDstr += 'X' + str(PD[-1]) + ']'
    else:
        PDstr += str(PD[-1]) + ']'
    return PDstr

PDString = 'PD[X[4, 2, 5, 1], X[ 6, 4, 1, 3], X[2, 6, 3, 5]]'

Invariants = []
InvariantsDict = dict()
y = var("y")
t = var("t")

f = open("PDCodes.txt", 'r')
Results = open('Knotted Graphs.txt','w')
Errors = open('Errors.txt','w')
for line in f:
    print(InvariantsDict)
    key = line[1:line.index(']')+1]
    PDString = line[line.index(']') + 3:-2]
    PD = parsePD(PDString)
    print(key)
    for nodeIndex in range(len(PD)):
        try:
            PDs = resolveNode(PD,nodeIndex)
            jones = list(map(lambda i: mathematica('Jones[' + makeMathematicaPD(PDs[i]) + '][t]').sage(), range(4)))
            r = list(map(lambda j: generate_r(j), jones))
            r[2] = r[2] * y
            r[3] = r[3] * y
            final = factor(simplify(expand(r[0] + r[1] + r[2] + r[3]))/((y+1)*(y+2)))
            if final not in Invariants:
                Invariants.append(final)
                InvariantsDict[final] = [str(nodeIndex) + ' ' + str(PD)]
                print(key, nodeIndex, PD, final, len(Invariants))
                Results.write(str(key) + ' ' + str(nodeIndex) + ' ' + str(PD) + ' ' + str(final) + '\n')
            else:
                InvariantsDict[final].append(str(nodeIndex) + ' ' + str(PD))
        except:
            Errors.write(str(key) + ' ' + str(nodeIndex)+ '\n')
            print('Error at ',key, nodeIndex )

print(InvariantsDict)

f.close()
Results.close()
Errors.close()

# k0= [[6, 2, 7, 1], [14, 10, 15, 9], [10, 3, 11, 4], [2, 13, 3, 14], [12, 5, 13, 6], [16, 8, 1, 7], [4, 11, 5, 12], [8, 16, 9, 15]]
# print(makeMathematicaPD(k0))
# print(makeMathematicaPD(switchCrossing(k0,3)))
#print(makeMathematicaPD(typeOne(K,1)))
#print(makeMathematicaPD(typeTwo(K,1)))
#print(makeMathematicaPD(switchHandedness(K)))








