import forceDirectedGraph, math
import matplotlib.pyplot as pyplot
from matplotlib.path import Path
import matplotlib.patches as patches
import numpy 
from scipy.interpolate import splprep, splev

def make_crossing_pdcode_explicit(crossing, number_of_crossings):
    crossing[0] = (crossing[0],'I')
    crossing[2] = (crossing[2],'O')
    if (crossing[3]-crossing[1]) % number_of_crossings == 1:
        crossing[1] = (crossing[1],'I')
        crossing[3] = (crossing[3],'O')
    else:
        crossing[1] = (crossing[1],'O')
        crossing[3] = (crossing[3],'I')
    return crossing

def make_pdcode_explicit(pdcode):
    for crossing in pdcode:
        crossing = make_crossing_pdcode_explicit(crossing, len(pdcode))
    return pdcode

def reverse_crossing_orientation(edge):
    if edge[1] == 'I':
        return (edge[0],'O')
    else:
        return (edge[0],'I')

def get_cycles_from_pdcode(pdcode):
    ''' computes all 'cycles' within the knot.'''
    next_anticlockwise_edge_at_crossing_map = {}
    for crossing in pdcode:
        crossing = make_crossing_pdcode_explicit(crossing, len(pdcode))
        for index in range(4):
            next_anticlockwise_edge_at_crossing_map[crossing[index]] = crossing[(index+1)%4]

    cycles = []
    while next_anticlockwise_edge_at_crossing_map != {}:
        key, value = next_anticlockwise_edge_at_crossing_map.popitem()
        cycle = [key[0]]
        while value[0] != cycle[0]:
            cycle.append(value[0])
            value = next_anticlockwise_edge_at_crossing_map.pop(reverse_crossing_orientation(value))
        cycles.append(cycle)

    # modify one cycle to be the outside of the graph
    adjustment = 4
    cycles = cycles[- adjustment:] + cycles [:-adjustment]
    cycles= cycles[:-1] + list(map(lambda e:[e], cycles[-1]))

    return cycles

def get_graph_of_cycles(cycles):
    graph_of_cycles = forceDirectedGraph.Graph()
    graph_of_cycles.add_verticies(*list(range(len(cycles))))
    #print(cycles)
    for i in range(len(cycles)):
        for j in range(i+1, len(cycles)):
            for edge in cycles[i]:
                if edge in cycles[j]:
                    graph_of_cycles.add_edge(i, j, name=edge)
    return graph_of_cycles

def get_crossing_locations(cycles, graph_of_cycles):
    for e in graph_of_cycles.edges:
        i = e.vertex1.name
        j = e.vertex2.name
        knot_edge_no = list(set(cycles[i]) & set(cycles[j]))[0]
        knot_edge_location = (e.vertex1.location+e.vertex2.location)/2
        diff = e.vertex2.location - e.vertex1.location
        knot_edge_direction = forceDirectedGraph.Vector(diff.x[1], -diff.x[0])
        # calculate edge position

def get_knot(graph_of_cycles):
    edges = {}
    for edge in graph_of_cycles.edges:
        center = (edge.vertex1.location + edge.vertex2.location) / 2
        edges[edge.name] = center
    return edges

def get_crossing_indexes(x, y):
    crossing_indexes = []
    i , j = 0, 1
    while j < len(x)-1:
        while i+1 < j:
            if forceDirectedGraph.do_intersect((x[i], y[i]), 
                                               (x[i+1], y[i+1]), 
                                               (x[j], y[j]), 
                                               (x[j+1], y[j+1])
                                              ):
                crossing_indexes.append(i)
                crossing_indexes.append(j)
            i += 1
        j += 1
        i = 0
    crossing_indexes.sort()
    return crossing_indexes

def render_knot(knot, pdcode):
    arc_midpoints = numpy.array(list(map(lambda x: knot[x].x, range(1, len(knot)+1))) + [knot[1].x])

    tck, u = splprep(arc_midpoints.T, u=None, s=0.0, per=1) 
    precision = 100
    u_new = numpy.linspace(u.min(), u.max(), len(arc_midpoints) * precision)
    x_new, y_new = splev(u_new, tck, der=0)

    crossing_indexes = get_crossing_indexes(x_new, y_new)
    wrapped_crossing_indexes = [0] + crossing_indexes + [len(x_new)]

    colours = ['g', 'b', 'r', 'y', 'o']

    undercrossings = list(map(lambda x: x[0][0], pdcode))
    overcrossings = [x for x in range(1, len(pdcode) * 2 + 1) if x not in undercrossings]

    print(wrapped_crossing_indexes)
    print(undercrossings)
    print(overcrossings)
    for i in range(len(wrapped_crossing_indexes)-1):
        start, end = get_segment_start_and_end_points(i, overcrossings, undercrossings, wrapped_crossing_indexes)
        print(i, start, end)
        pyplot.plot(x_new[start:end], y_new[start:end], colours[i % 1] + '-')
    pyplot.show()

def get_segment_start_and_end_points(i, overcrossings, undercrossings, wrapped_crossing_indexes):
    if i in undercrossings:
        start = wrapped_crossing_indexes[i] + 12
    else:
        start = wrapped_crossing_indexes[i]
    if i+1 in undercrossings:
        end = wrapped_crossing_indexes[i+1] - 12
    else:
        end = wrapped_crossing_indexes[i+1] + 1
    return start, end

def draw_pdcode(pdcode):
    cycles = get_cycles_from_pdcode(pdcode)
    graph_of_cycles = get_graph_of_cycles(cycles)
    graph_of_cycles.calculate_valid_graph(view=True)
    knot = get_knot(graph_of_cycles)
    render_knot(knot, pdcode)


if __name__ == '__main__':

    #pdcode=[[1, 4, 2, 5], [3, 6, 4, 1], [5, 2, 6, 3]]
    pdcode = [[4, 2, 5, 1], [8, 6, 1, 5], [6, 3, 7, 4], [2, 7, 3, 8]]
    #pdcode = [[1, 6, 2, 7], [3, 8, 4, 9], [5, 10, 6, 1], [7, 2, 8, 3], [9, 4, 10, 5]]
    #pdcode = [[6, 2, 7, 1], [16, 11, 17, 12], [10, 4, 11, 3], [2, 15, 3, 16], [14, 5, 15, 6], [18, 8, 1, 7], [4, 10, 5, 9], [12, 17, 13, 18], [8, 13, 9, 14]]

    draw_pdcode(pdcode)


